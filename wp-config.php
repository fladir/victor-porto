<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'victorporto' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'ciaserver_user' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'ciaserver_mysql' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<l<R>@/TOdgr2)@n.[sDas6f!>*7{eZv%ad#uId0&$H{Uyf$sn>*+|XE0!uN$e@C' );
define( 'SECURE_AUTH_KEY',  '5aV(Z/C%UEEn;aO-C>W>=&Y{o2e>>ILXxF8I>Q2 x:lu_[ETGn>VW@/Gh$dyB]u,' );
define( 'LOGGED_IN_KEY',    'rir{?}{%0GF$wfrzIi%%O~BJ[9cfPa%@ 7vKS9@]?69.:&@tcO04i }ck7J,!:ws' );
define( 'NONCE_KEY',        'xyXC!q1jWGVq2^kU*xK`L$*Sv/S@gl;#p}zvDRg$}?Dep}.i CyyL_=CYb}xJ9a,' );
define( 'AUTH_SALT',        'W[MjSb5H-U!2VsE1{[Quu0{tXB9OfrIV&-T.m2Xen($FkD*Ust4>*_<d<I =Y`YK' );
define( 'SECURE_AUTH_SALT', '1>F6(KGR^$d89Q,x&Sjp|u~h6wR&p&p4BSwB$E}]%E2E=Sp0La,iEJ>HsSD._LA/' );
define( 'LOGGED_IN_SALT',   'RmWEHO>AHGH4_+.s+LnO Yd;$v]GMkD$sKxfC)sw<AKqw4!pRwTdQ XAnh-u(}O$' );
define( 'NONCE_SALT',       'a-$:M)]yJ~+zuKk%238pBTU/xGaMXygDiVT05qD:Zhig.^;RhEpJ&Nq2=P,#7,,b' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'vp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
