<?php
get_header();
?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>
    <section id="pagina">
        <div class="container">
            <?php the_content(); ?>
        </div>
    </section>


    <!-- Calculadora de IMC -->
<?php get_template_part('components/calculadora-imc/calculadora-imc'); ?>

    <!-- Dr. na Mídia -->
<?php get_template_part('components/index/dr-na-midia'); ?>

<?php get_footer(); ?>