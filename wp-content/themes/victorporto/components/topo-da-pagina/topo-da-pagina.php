<?php
$grupoTopoDaPaginaGeral = get_field('grupo_header', 'options')['imagem_de_fundo_geral'];
$grupoTopoDaPagina = get_field('imagem_de_fundo');
?>

<section id="topo-da-pagina">
    <img src="<?php $grupoTopoDaPagina ? print_r($grupoTopoDaPagina['sizes']['topo_da_pagina']) : print_r($grupoTopoDaPaginaGeral['sizes']['topo_da_pagina'])  ?>"
         alt="<?php the_title() ?>" title="<?php the_title() ?>" class="bg-topo-da-pagina">

    <div class="container">
        <div class="row d-flex align-items-end">

            <div class="col-12 text-left">

                <?php if(is_home()) : ?>
                    <h1 class="fw-semi-bold text-uppercase text-white">Blog</h1>
                <?php elseif(is_search() && have_posts()) : ?>
                    <h1 class="fw-semi-bold text-uppercase text-white"><?php printf(__('Resultados para: %s', ''), get_search_query()); ?></h1>
                <?php elseif(is_search() && !have_posts()) : ?>
                    <h1 class="fw-semi-bold text-uppercase text-white"><?php printf(__('Nenhum resultado para: %s', ''), get_search_query()); ?></h1>
                <?php elseif(is_404()) : ?>
                    <h1 class="fw-semi-bold text-uppercase text-white">Erro 404</h1>
                <?php elseif ( is_page_template( 'templates/denuncias.php' ) || is_page_template( 'templates/orcamento.php' ) || is_page_template( 'templates/criticas-e-sugestoes.php' ) ) : ?>
                    <h1 class="fw-semi-bold text-uppercase text-white">Fale Conosco</h1>
                <?php else : ?>
                <h1 class="fw-semi-bold text-uppercase text-white <?php if(is_single()){echo 'single-title';} ?>"><?php the_title() ?></h1>
                <?php endif; ?>
                <div class="mt-3">
                <?php
                if (function_exists('yoast_breadcrumb')) {
                    yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
                }
                ?>
                </div>

            </div>
        </div>
    </div>
</section>
