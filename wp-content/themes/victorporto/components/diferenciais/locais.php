<?php
$locais = get_field('locais');
?>

<section id="locais">
    <div class="container">
        <div class="row">
            <?php if ($locais) : foreach ($locais as $local) : ?>
                <div class="col-md-4 mb-5">
                    <div class="conteudo d-flex align-content-center">
                        <img src="<?php echo get_template_directory_uri() . '/assets/img/local.png'; ?>"
                             alt="<?php the_title() ?>" class="icon-local">
                        <div class="d-flex flex-column ml-3">
                            <h4 class="text-primario fw-bold mb-1"><?php echo $local['local'] ?></h4>
                            <p class="fw-semi-bold"><?php echo $local['descricao'] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </div>
</section>
