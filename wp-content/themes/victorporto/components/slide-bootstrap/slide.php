<?php
$slides = get_field('slide_repetidor');
if (have_rows('grupo_conteudos_dos_componentes', 'options')): while (have_rows('grupo_conteudos_dos_componentes', 'options')) : the_row();
    $i = 0;
    if (have_rows('slide_repetidor')): while (have_rows('slide_repetidor')) : the_row();
        $i++;
    endwhile; endif;
endwhile; endif;
?>
<?php if ($slides) : ?>
    <section class="slides">
        <div id="slide-bootstrap" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators justify-content-center">
                <?php
                if ($i > 1) :
                    foreach ($slides as $key => $slide) : ?>
                        <li data-target="#slide-bootstrap" data-slide-to="<?php echo $key; ?>"
                            class="<?php echo $key == 0 ? "active" : "" ?>"></li>
                    <?php endforeach; endif; ?>
            </ol>

            <div class="carousel-inner">

                <?php foreach ($slides as $key => $slide) : ?>
                    <div class="carousel-item <?php echo $key == 0 ? 'active' : '' ?>">
                        <!-- Imagem -->
                        <img src="<?php print_r($slide['imagem_mobile']['sizes']['slide_mobile']) ?>"
                             alt="<?php the_title() ?>" title="<?php the_title() ?>"
                             class="slide-mobile">

                        <img src="<?php print_r($slide['imagem']['sizes']['slides']) ?>"
                             alt="<?php the_title() ?>" title="<?php the_title() ?>"
                             class="slide-desktop">

                        <div class="container">
                            <div class="row mt-md-5">
                                <div class="col-md-7 text-left pr-md-4 pt-md-5 mt-md-5 slide-content animated fadeInLeft">
                                    <h2 class="text-left text-white"><?php echo $slide['titulo'] ?></h2>
                                    <p class="text-slide"><?php echo $slide['texto'] ?></p>
                                    <?php if ($slide['link_botao']) : ?>

                                        <a href="<?php echo $slide['link_botao']; ?>"
                                           class="btn btn-primario">
                                            <?php echo $slide['texto_botao']; ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
            <?php if ($i > 1) : ?>
                <a class="carousel-control-prev" href="#slide-bootstrap" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only"> Previous</span>
                </a>
                <a class="carousel-control-next" href="#slide-bootstrap" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only"> Next</span>
                </a>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>