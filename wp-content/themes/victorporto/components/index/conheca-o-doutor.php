<?php
$conhecaODoutor = get_field('conheca_o_doutor');
#echo '<pre>'; print_r($conhecaODoutor); echo '</pre>';
?>

<section id="conheca-o-doutor">
    <img src="<?php print_r($conhecaODoutor['imagem']['sizes']['img_full']) ?>"
         alt="<?php echo $conhecaODoutor['titulo'] ?>" title="<?php echo $conhecaODoutor['titulo'] ?>"
         class="conheca-o-dr-bg">

    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h3 class="text-uppercase mb-5 text-primario fw-semi-bold"><?php echo $conhecaODoutor['titulo'] ?></h3>

                <?php echo $conhecaODoutor['texto'] ?>

                <a href="<?php echo $conhecaODoutor['link_do_botao']; ?>"
                   class="btn btn-destaque mt-4">
                    <?php echo $conhecaODoutor['texto_do_botao']; ?>
                </a>

            </div>
        </div>
    </div>
</section>