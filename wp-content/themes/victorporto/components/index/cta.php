<?php
$cta = get_field('call_to_action')
?>

<section id="cta-home">
    <div class="container-fluid p-0 wow fadeIn">
        <div class="row">
            <div class="col-12">
                <img src="<?php print_r($cta['imagem_de_fundo']['sizes']['cta_home']) ?>"
                     alt="<?php echo $cta['chamada'] ?>" title="<?php echo $cta['chamada'] ?>">
                <span class="overlay"></span>
                <div class="conteudo text-center">
                    <h2 class="text-white fw-bold mb-4"><?php echo $cta['chamada'] ?></h2>
                    <a href="<?php echo $cta['link_do_botao'] ?>" class="btn btn-primario animated fadeInUp"><?php echo $cta['texto_do_botao'] ?></a>
                </div>

            </div>
        </div>
    </div>
</section>
