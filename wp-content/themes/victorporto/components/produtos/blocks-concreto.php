<?php
$produtos = get_field('produtos');
$args = array(
    'nopaging' => false,
    'post_type' => 'produto',
    'posts_per_page' => 8,
    'order' => 'ASC',
    'tax_query' => array(
        array(
            'taxonomy' => 'categoria-produto',   // taxonomy name
            'terms' => 6,                  // term id, term slug or term name
        )
    )
);
$WPQuery = new WP_Query($args);
?>

<section id="blocks">
    <div class="container">
        <div class="row">
            <?php if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                <div class="col-md-4 mb-4 wow fadeIn">
                    <div class="content text-center d-flex flex-column justify-content-around">
                        <h4 class="text-primario fw-bold text-left mb-4"><?php the_title() ?></h4>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>" class="btn btn-bordered mt-3 w-75">
                            Saiba Mais
                        </a>
                    </div>
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</section>