<section class="bianca-produtos">
    <div class="container">
        <div class="row">

            <?php
            $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
            $descriptions = get_field('caracteristicas_do_produto'); ?>
            <?php if ($descriptions) : ?>
                <?php foreach ($descriptions as $key => $description) : ?>
                    <?php if ($description['tipo_de_exibicao'] == 1) : ?>

                        <div class="descricao horizontal-half col-12 col-md-6 wow fadeInLeft">
                            <figure>
                                <?php if ($description['imagem']) : ?>
                                    <?php echo wp_get_attachment_image($description['imagem'], 'horizontal-half', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                <?php else : ?>
                                    <?php echo wp_get_attachment_image($img_padrao, 'horizontal-half', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                <?php endif; ?>
                            </figure>
                            <h2><?php echo $description['titulo']; ?></h2>
                            <?php echo $description['texto']; ?>
                        </div>

                    <?php elseif ($description['tipo_de_exibicao'] == 2) : ?>

                        <div class="descricao vertical-half col-12 col-md-6 wow fadeInRight">
                            <figure>
                                <?php if ($description['imagem']) : ?>
                                    <?php echo wp_get_attachment_image($description['imagem'], 'vertical-half', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                <?php else : ?>
                                    <?php echo wp_get_attachment_image($img_padrao, 'vertical-half', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                <?php endif; ?>
                            </figure>
                            <h2><?php echo $description['titulo']; ?></h2>
                            <p><?php echo $description['texto']; ?></p>
                        </div>

                    <?php elseif ($description['tipo_de_exibicao'] == 3) : ?>

                        <div class="descricao horizontal-full col-12 wow fadeInLeft">
                            <div class="row">
                                <div class="col-12 col-md-7">
                                    <figure>
                                        <?php if ($description['imagem']) : ?>
                                            <?php echo wp_get_attachment_image($description['imagem'], 'horizontal-full', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                        <?php else : ?>
                                            <?php echo wp_get_attachment_image($img_padrao, 'horizontal-full', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                        <?php endif; ?>
                                    </figure>
                                </div>
                                <div class="col-12 col-md-5">
                                    <h2><?php echo $description['titulo']; ?></h2>
                                    <?php echo $description['texto']; ?>
                                </div>
                            </div>
                        </div>
                        
                    <?php elseif ($description['tipo_de_exibicao'] == 4) : ?>

                        <div class="descricao col-12 bg-infinito wow fadeInRight">
                            <div class="row">
                                <div class="col-12 col-md-6 d-flex flex-column justify-content-center">
                                    <div class="col-12 col-md-10 offset-md-1 d-flex flex-column justify-content-center">
                                        <h2><?php echo $description['titulo']; ?></h2>
                                        <?php echo $description['texto']; ?>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 d-flex justify-content-center">
                                    <figure>
                                        <?php if ($description['imagem']) : ?>
                                            <?php echo wp_get_attachment_image($description['imagem'], 'vertical-full', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                        <?php else : ?>
                                            <?php echo wp_get_attachment_image($img_padrao, 'vertical-full', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                        <?php endif; ?>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            
        </div>
    </div>
</section>