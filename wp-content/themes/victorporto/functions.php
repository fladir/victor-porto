<?php

add_filter('show_admin_bar', '__return_false');

# CIA SETTINGS

Add_filter('show_admin_bar', '__return_false');

$themename = "Configurações do Site";
define('THEME_NAME', $themename);
define('THEME_FOLDER', "ciawebsites");
define('THEME_VER', 5);

# PBO SETTINGS
require_once get_template_directory() . '/core/index.php';
require_once(get_template_directory() . '/functions/custom-types.php');

// Chave da API do Maps
function my_acf_init()
{
    acf_update_setting('google_api_key', 'AIzaSyCxQHlF-7oXndl6laoEGZCzQ9kho7HhJzQ');
}

add_action('acf/init', 'my_acf_init');


add_action('widgets_init', 'my_recent_widget_registration');

function joki_count_views($postID) {
    $post_meta = 'joki_post_views_count';
    $count = get_post_meta($postID, $post_meta, true);
    if($count == '') {
        $count = 0;
        delete_post_meta($postID, $post_meta);
        add_post_meta($postID, $post_meta, '0');
    }
    else {
        $count++;
        update_post_meta($postID, $post_meta, $count);
    }
}

function joki_track_views ($post_id) {
    if ( !is_single() ) { return; }
    if ( empty ( $postId) ) {
        global $post;
        $postId = $post->ID;
    }
    joki_count_views($postId);
}
add_action( 'wp_head', 'joki_track_views');
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10);


function ellipses_excerpt_more( $output ) {
    if ( has_excerpt() && ! is_attachment() ) {
        $output .= '...';
    }
    return $output;
}
add_filter( 'get_the_excerpt', 'ellipses_excerpt_more', 1, 10 );

function word_count($string, $limit) {

    $words = explode(' ', $string);

    return implode(' ', array_slice($words, 0, $limit));

}