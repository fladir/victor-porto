<?php get_header(); ?>
    <!-- Topo -->
<?php  get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <section id="404">
        <div class="container">
            <div class="row">
                <div class="col-12 py-5 my-5 text-center">
                    <h2 class="text-dark fw-bold mb-5">Página não encontrada!</h2>
                    <a href="<?php echo get_site_url() ?>" class="btn btn-primario mb-5">Ir para a Home</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Call to Action -->
<?php  get_template_part('components/call-to-action/cta'); ?>

<?php get_footer(); ?>