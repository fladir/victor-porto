<?php
/* Template Name: Contato */
get_header();
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
$whatsapps = get_field('grupo_informacoes_para_contato', 'options')['whatsapp'];
$redesSociais = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
$conteudoContato = get_field('conteudo_contato');
?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>
    <section id="contato">
        <div class="container">
            <div class="row">
                <div class="col-md-7 pr-md-5">
                    <h4 class="text-uppercase text-secundario fw-bold mb-4">
                        <?php echo $conteudoContato['frase_destaque'] ?>
                    </h4>
                    <div class="social-media">
                    <span class="telefone mr-2 mb-2">
                        <span class="icon-wrapper">
                    <i class="fas fa-phone-alt mr-2 "></i>
                        </span>
                    <?php foreach ($telefones as $telefone) : ?>
                        <a class="mr-2" href="tel:<?php echo $telefone['numero_telefone']; ?>" target="_blank">
                    <?php echo $telefone['numero_telefone']; ?>
                    </a>
                    <?php endforeach; ?>
                </span>

                        <?php foreach ($whatsapps as $whatsapp) : ?>
                            <span class="whatsapp mr-2 mb-2">
                        <span class="icon-wrapper">
                    <i class="fab fa-whatsapp mr-2 "></i>
                        </span>
                    <a href="https://api.whatsapp.com/send?phone=55<?php echo $whatsapp['link_whatsapp']; ?>&text=Ola,%20tudo%20bem?"
                       target="_blank">
                    <?php echo $whatsapp['numero_whatsapp']; ?>
                    </a>
                </span>
                        <?php endforeach; ?>

                        <?php foreach ($emails as $email) : ?>
                            <span class="email mr-2 mb-2">
                        <span class="icon-wrapper">
                    <i class="fas fa-envelope mr-2 "></i>
                        </span>
                    <a href="mailto:<?php echo $email['endereco_email']; ?>" target="_blank">
                    <?php echo $email['endereco_email']; ?>
                    </a>
                </span>
                        <?php endforeach; ?>

                        <?php foreach ($enderecos as $endereco) : ?>
                            <span class="endereco mr-2 mb-2">
                        <span class="icon-wrapper">
                            <i class="fas fa-map-marker-alt mr-2"></i>
                        </span>
                        <span>
                        <?php echo $endereco['endereco']; ?>
                        <?php echo $endereco['bairro']; ?>
                        <?php echo $endereco['cidade_estado']; ?>
                            </span>
                    </span>
                        <?php endforeach; ?>
                    </div>
                    <div class="mt-4 pr-md-5 mr-md-5 mb-5">
                        <?php echo do_shortcode('[contact-form-7 title="Contato"]'); ?>
                    </div>

                </div>
                <div class="col-md-5">
                    <img src="<?php print_r($conteudoContato['imagem_lateral']['sizes']['img_contato']) ?>"
                         alt="<?php the_title() ?>" title="<?php the_title() ?>"
                         class="contact-sidebar">

                </div>

            </div>
        </div>
    </section>

<?php get_footer(); ?>