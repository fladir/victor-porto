<?php
/* Template Name: Dr. Girundi na Mídia */
get_header();
$items = get_field('items_entrevistas');
?>
    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>
    <section id="dr-na-midia-page">
        <div class="container">
            <?php if ($items) : ?><div class="row galeria-de-imagens">
                <div class="col-12 col-md-6 mb-3">
                    <h4 class="text-secundario text-uppercase fw-bold mb-5"><?php echo get_field('titulo_da_secao') ?></h4>
                </div>
                </div>
                <div class="row galeria-de-imagens">
                    <?php foreach ($items as $item) :
                        $imagem = $item['imagem_de_pre-visualizacao']['sizes']['img_entrevistas'];
                        $link = $item['link_do_video'];
                        $origemDoVideo = $item['origem_do_video'];
                        $origem = $origemDoVideo == 'YouTube' ? 'data-toggle="lightbox"' : '';
                        ?>
                        <div class="col-6 col-md-4 mb-5 img-portfolio-wrapper d-flex justify-content-center flex-column">
                            <h5 class="text-secundario text-center titulo-interviews"><?php echo $item['titulo_entrevista'] ?></h5>
                            <div class="card border-0">
                                <a class="link-imagem-portfolio"
                                   href="<?php echo esc_url($link); ?>"
                                    <?php echo $origem ?>
                                   data-gallery="example-gallery" target="_blank">
                                    <figure class="figure">
                                        <img src="<?php echo esc_url($imagem); ?>"
                                             class="img-fluid img-portfolio lazyload">
                                        <span class="overlay d-flex justify-content-center align-items-center">
                                            <i class="fas fa-play"></i>
                                        </span>
                                    </figure>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </section>

    <!-- Calculadora de IMC -->
<?php get_template_part('components/calculadora-imc/calculadora-imc'); ?>

<div style="height: 80px"></div>

<?php get_footer(); ?>