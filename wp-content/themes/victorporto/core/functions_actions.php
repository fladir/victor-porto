<?php

add_action('init', 'myStartSession', 1);
function myStartSession()
{
    if (!session_id()) {
        session_start();
    }
}

# Adicionando menus ao menu principal do painel adiminstrativo
function admin_bar_item(WP_Admin_Bar $admin_bar)
{
    if (!current_user_can('manage_options')) {
        return;
    }

    $args = array(
        'id' => 'cia_menu',
        'title' => __('<span class="cia-logo"> <img  src="' . get_template_directory_uri() . '/core/assets/img/criacao-de-sites.png"></span>'),
        'href' => 'http://www.ciawebsites.com.br'
    );
    $admin_bar->add_node($args);

    $args = array(
        'id' => 'cia_menu_criacao',
        'parent' => 'cia_menu',
        'title' => __('Criação de Sites'),
        'href' => 'http://www.ciawebsites.com.br/solucoes/criacao-de-sites/'
    );
    $admin_bar->add_node($args);

    $args = array(
        'id' => 'cia_menu_otimizacao',
        'parent' => 'cia_menu',
        'title' => __('Otimização de Sites'),
        'href' => 'http://www.ciawebsites.com.br/solucoes/otimizacao-de-sites-seo/'
    );
    $admin_bar->add_node($args);

    $args = array(
        'id' => 'cia_menu_lojas',
        'parent' => 'cia_menu',
        'title' => __('Lojas Virtuais'),
        'href' => 'http://www.ciawebsites.com.br/solucoes/criacao-de-lojas-virtuais/'
    );
    $admin_bar->add_node($args);
}

add_action('admin_bar_menu', 'admin_bar_item', 500);


# Remover Dashboard widgets
function remove_dashboard_widgets()
{
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['wpseo-dashboard-overview']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

// Customizar o Footer do WordPress
function remove_footer_admin()
{
    echo '© <a href="http://www.ciawebsites.com.br/">Cia Web Sites</a> - Criação e otimização de sites';
}

add_filter('admin_footer_text', 'remove_footer_admin');

# PÁGINA DE LOGIN
function change_title_on_logo()
{
    return 'Voltar para ' . get_bloginfo('name');
}

add_filter('login_headertext ', 'change_title_on_logo');

##########//Remover códigos
//head
remove_action('wp_head', 'rsd_link'); //removes EditURI/RSD (Really Simple Discovery) link.
remove_action('wp_head', 'wlwmanifest_link'); //removes wlwmanifest (Windows Live Writer) link.
remove_action('wp_head', 'wp_generator'); //removes meta name generator.
remove_action('wp_head', 'wp_shortlink_wp_head'); //removes shortlink.
remove_action('wp_head', 'feed_links', 2); //removes feed links.
remove_action('wp_head', 'feed_links_extra', 3);  //removes comments feed.
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_resource_hints', 2);

require_once(get_template_directory() . '/mail/processar-envio.php');

function arphabet_widgets_init()
{

    register_sidebar(array(
        'name' => 'Blog Sidebar',
        'id' => 'blog_sidebar',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded fw-bold">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => 'Sidebar Produtos',
        'id' => 'sidebar_produtos',
        'before_widget' => '<div class="widgets">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded fw-bold">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => 'Rodapé Posts',
        'id' => 'rodape_posts',
        'before_widget' => '<div class="widgets">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded fw-bold">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'arphabet_widgets_init');


//Start Recents Posts
Class My_Recent_Posts_Widget extends WP_Widget_Recent_Posts
{

    function widget($args, $instance)
    {

        if (!isset($args['widget_id'])) {
            $args['widget_id'] = $this->id;
        }

        $title = (!empty($instance['title'])) ? $instance['title'] : __('Recent Posts');

        /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
        $title = apply_filters('widget_title', $title, $instance, $this->id_base);

        $number = (!empty($instance['number'])) ? absint($instance['number']) : 5;
        if (!$number)
            $number = 5;
        $show_date = isset($instance['show_date']) ? $instance['show_date'] : false;

        /**
         * Filter the arguments for the Recent Posts widget.
         *
         * @param array $args An array of arguments used to retrieve the recent posts.
         * @see WP_Query::get_posts()
         *
         * @since 3.4.0
         *
         */


        $r = new WP_Query(apply_filters('widget_posts_args', array(
            'posts_per_page' => 3,
            'meta_key' => 'joki_post_views_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC'
        )));


        if ($r->have_posts()) :
            ?>
            <?php echo $args['before_widget']; ?>
            <?php if ($title) {
            echo $args['before_title'] . $title . $args['after_title'];
        } ?>

            <?php while ($r->have_posts()) : $r->the_post(); ?>


            <div class="row widget-mais-lidos mb-3">
                <div class="col-6 pr-md-2">
                    <a href="<?php echo get_the_permalink() ?>">
                        <?php the_post_thumbnail('img-mais-lidos', array('class' => 'img-recents-posts', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                    </a>
                </div>
                <div class="col-6 pl-md-0">
                    <a href="<?php echo get_the_permalink() ?>">
                        <h4 class="truncate4 title-mais-lidos"
                            title="<?php get_the_title() ? the_title() : the_ID(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?> </h4>
                    </a>
                </div>
            </div>
        <?php endwhile; ?>

            <?php echo $args['after_widget']; ?>
            <?php
            // Reset the global $the_post as this query will have stomped on it
            wp_reset_postdata();

        endif;

    }
}

//End Recents Posts

function my_recent_widget_registration()
{
    unregister_widget('WP_Widget_Recent_Posts');
    register_widget('My_Recent_Posts_Widget');
}

#Widget banner blog
include get_template_directory() . '/core/functions/widget-banner-blog.php';

function admin_theme_style()
{
    wp_enqueue_style( 'admin-theme', get_stylesheet_directory_uri() . '/assets/css/wp-admin.css' );
}
add_action('admin_enqueue_scripts', 'admin_theme_style');
add_action('login_enqueue_scripts', 'admin_theme_style');