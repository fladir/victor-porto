<?php
# FRONT END
function pbo_load_css()
{
    wp_enqueue_style('customCSS', get_template_directory_uri() . '/assets/css/style.css', array());
    wp_enqueue_style('fontAwesome', get_template_directory_uri() . '/node_modules/@fortawesome/fontawesome-free/css/all.min.css', array());
    wp_enqueue_style('PoppinsFont', '//fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap', array());
    wp_enqueue_style('AnimateCSS', get_template_directory_uri() . '/node_modules/animate.css/animate.min.css', array());
    wp_enqueue_style('owlCarouselCSS', get_template_directory_uri() . '/node_modules/owl.carousel/dist/assets/owl.carousel.min.css', array());          
    wp_enqueue_style('owlCarouselTheme', get_template_directory_uri() . '/node_modules/owl.carousel/dist/assets/owl.theme.default.min.css', array());          
    wp_enqueue_style('select2', get_template_directory_uri() . '/node_modules/select2/dist/css/select2.min.css', array());
    wp_enqueue_style('fancyBox', get_template_directory_uri() . '/node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css', array());
    wp_enqueue_style('ekkoLightbox', get_template_directory_uri() . '/node_modules/ekko-lightbox/dist/ekko-lightbox.css', array());

}

add_action('wp_enqueue_scripts', 'pbo_load_css');

# BACK END

function load_admin_css()
{
    wp_enqueue_style('style_admin', get_template_directory_uri() . '/core/assets/css/style_admin.css', array());
}

add_action('admin_head', 'load_admin_css');


function load_login_css()
{
    wp_enqueue_style('style_login', get_template_directory_uri().'/core/assets/css/style_login.css');
}
add_action('login_enqueue_scripts', 'load_login_css');