<?php
// Enable post thumbnails
add_theme_support('post-thumbnails');

// Custom image sizes
if (function_exists('add_image_size')) {
    add_image_size('logo', 160, 106, true); // Logo
    add_image_size('slides', 1920, 876, true); // Slides
    add_image_size('slide_mobile', 410, 630, true); // Slide Mobile
    add_image_size('col_6', 960, 630, true); // Coluna (Metade)
    add_image_size('topo_da_pagina', 1920, 320, true); // Topo da Página
    add_image_size('img_full', 1920, 520); // Imagem Fullwidth
    add_image_size('thumb_galeria', 320, 220, true); // Thumb Galeria
    add_image_size('full_galeria', 800, 600, true); // img Full Galeria
    add_image_size('img_post_list', 350, 205, true); // Imagem Posts List
    add_image_size('img_entrevistas', 350, 245, true); // Imgs Entrevistas
    add_image_size('img_secoes_alternadas', 550, 310, true); // Seções Alternadas
    add_image_size('img_secoes_alternadas', 550, 310, true); // Seções Alternadas
    add_image_size('fundo_secao', 1920, 690, true); // Fundo Seção
    add_image_size('topo_da_pagina', 1920, 560, true); // Topo da página
    add_image_size('icone_institucional', 62, 62); // Ícone Institucional
    add_image_size('img_contato', 430, 650); // Pág. Contato



}