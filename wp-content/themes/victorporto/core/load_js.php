<?php

function pbo_load_js()
{       
    # wp_enqueue_script('loadingJS', get_template_directory_uri() . '/core/components/loading/loading.js', 1, false);  

    
    # Custom JS
    wp_enqueue_script('Global-Js', get_template_directory_uri() . '/assets/js/global.js', array('jquery'), 1, true);
    wp_enqueue_script('jQueryMigrate', get_template_directory_uri() . '/assets/libs/js/jquery-migrate-3.0.0.min.js', array('jquery'), 1, true);
    wp_enqueue_script('popper', get_template_directory_uri() . '/assets/libs/js/popper.min.js', array('jquery'), 1, true);
    wp_enqueue_script('jQueryMMenu', get_template_directory_uri() . '/assets/libs/js/jquery.mmenu.all.js', array('jquery'), 1, true);
    wp_enqueue_script('aceResponsiveMenu', get_template_directory_uri() . '/assets/libs/js/ace-responsive-menu.js', array('jquery'), 1, true);
    wp_enqueue_script('bootstrapSelect', get_template_directory_uri() . '/assets/libs/js/bootstrap-select.min.js', array('jquery'), 1, true);
    wp_enqueue_script('isotop', get_template_directory_uri() . '/assets/libs/js/isotop.js', array('jquery'), 1, true);
    wp_enqueue_script('snackbar', get_template_directory_uri() . '/assets/libs/js/snackbar.min.js', array('jquery'), 1, true);
    wp_enqueue_script('simplebar', get_template_directory_uri() . '/assets/libs/js/simplebar.js', array('jquery'), 1, true);
    wp_enqueue_script('parallax', get_template_directory_uri() . '/assets/libs/js/parallax.js', array('jquery'), 1, true);
    wp_enqueue_script('scrollTo', get_template_directory_uri() . '/assets/libs/js/scrollto.js', array('jquery'), 1, true);
    wp_enqueue_script('scrollFixed', get_template_directory_uri() . '/assets/libs/js/jquery-scrolltofixed-min.js', array('jquery'), 1, true);
    wp_enqueue_script('counterUp', get_template_directory_uri() . '/assets/libs/js/jquery.counterup.js', array('jquery'), 1, true);
    wp_enqueue_script('wow', get_template_directory_uri() . '/assets/libs/js/wow.min.js', array('jquery'), 1, true);
    wp_enqueue_script('timePicker', get_template_directory_uri() . '/assets/libs/js/timepicker.js', array('jquery'), 1, true);
    wp_enqueue_script('scripts', get_template_directory_uri() . '/assets/libs/js/script.js', array('jquery'), 1, true);



    wp_enqueue_script('utilsCore', get_template_directory_uri() . '/core/assets/js/utilsFramework.js', array('jquery'), 1, true);     
}

add_action('wp_enqueue_scripts', 'pbo_load_js');
