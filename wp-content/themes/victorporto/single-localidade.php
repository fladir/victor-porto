<?php
get_header();
$grupoTopoDaPaginaGeral = get_field('grupo_conteudos_dos_componentes', 'options')['imagem_de_fundo'];
$cidades = get_field('cidade');
#echo '<pre>'; print_r($cidades); echo '</pre>';

?>

    <!-- Topo -->
    <section id="topo-da-pagina">
        <img src="<?php print_r($grupoTopoDaPaginaGeral['sizes']['topo_da_pagina']) ?>"
             alt="<?php the_title() ?>" title="<?php the_title() ?>" class="bg-topo-da-pagina">

        <div class="container">
            <div class="row d-flex align-items-end">

                <div class="col-md-6 text-left">
                    <h1 class="text-primario fw-bold">Onde Estamos</h1>
                </div>
                <div class="col-md-6 text-right">
                    <?php
                    if (function_exists('yoast_breadcrumb')) {
                        yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Post -->
    <section id="single-localidade">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-12 content">
                        <h2 class="titulo mb-5 w-25 fw-bold">
                            <?php the_title(); ?>
                        </h2>
                        <?php the_content(); ?>
                    </div>
                </div>
                <?php get_template_part('/components/onde-estamos/locais'); ?>
            </div>
        </article>
    </section>

<?php get_template_part('components/index/produtos'); ?>

<?php get_template_part('/components/call-to-action/cta'); ?>

<?php get_footer(); ?>