<?php get_header(); ?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <!-- Post -->
    <section id="blog">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 img-post-principal">
                        <?php if (has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail('blog-destaque', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-8 offset-md-2 content-post">
                        <?php the_content(); ?>

                        <h3 class="mt-5">COMPARTILHAR</h3>
                        <ul class="compartilhar-post">
                            <li class="facebook">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink() ?>">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                            <li class="whatsapp">
                                <a href="https://api.whatsapp.com/send?phone=&text=<?php echo get_the_permalink() ?>">
                                    <i class="fab fa-whatsapp"></i>
                                </a>
                            </li>
                            <li class="email">
                                <a href="mailto:?&subject=&body=<?php echo get_the_permalink() ?>">
                                    <i class="fas fa-envelope"></i>
                                </a>
                            </li>
                            <li class="twitter">
                                <a href="https://twitter.com/home?status=<?php echo get_the_permalink() ?>">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="linkedin">
                                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_the_permalink() ?>&title=&summary=&source=">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </article>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <hr class="hr-tracejado">
            </div>
        </div>
    </div>
    <section id="rodape-posts">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mb-4 text-dark fw-bold">
                        Posts Relacionados
                    </h2>
                </div>
                <div class="col-md-8 posts-relacionados">

                    <?php
                    //for use in the loop, list 5 post titles related to first tag on current post
                    $tags = wp_get_post_tags($post->ID);
                    if ($tags) {
                        $first_tag = $tags[0]->term_id;
                        $args = array(
                            'tag__in' => array($first_tag),
                            'post__not_in' => array($post->ID),
                            'posts_per_page' => 2,
                            'caller_get_posts' => 1
                        );
                        ?>
                        <div class="row mb-5">
                        <?php $my_query = new WP_Query($args);
                        if ($my_query->have_posts()) {
                            while ($my_query->have_posts()) : $my_query->the_post(); ?>

                                <div class="col-md-6 mb-4">
                                    <a class="link-imagem-recents-post" href="<?php echo get_the_permalink() ?>">
                                        <figure>
                                            <?php the_post_thumbnail('img_post_list', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                        </figure>
                                    </a>


                                    <a class="link-titulo-recents-post" href="<?php echo get_the_permalink() ?>">
                                        <h2 class="text-left py-3"><?php echo get_the_title() ?></h2>
                                    </a>
                                    <a class="btn btn-secundario" href="<?php echo get_the_permalink() ?>">Leia
                                        Mais</a>
                                </div>


                            <?php
                            endwhile; ?>
                            </div>
                            <?php
                        }
                        wp_reset_query();
                    }
                    ?>
                </div>
                <div class="col-md-4">
                    <?php dynamic_sidebar('rodape_posts'); ?>

                </div>
            </div>
        </div>
    </section>

    <!-- Call to Action -->
<?php  get_template_part('components/call-to-action/cta'); ?>

<?php get_footer(); ?>