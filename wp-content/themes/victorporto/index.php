<?php
/* Template Name: Blog */
get_header();
?>

    <!-- Topo -->
<?php  get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

<?php get_template_part('/components/blog/blog'); ?>

    <!-- Call to Action -->
<?php  get_template_part('components/call-to-action/cta'); ?>



<?php get_footer(); ?>