(function ($) {
    $(document).ready(function () {
        // Carousel Tratamentos Cirúrgicos
        $(".tratamentos-cirurgicos-carousel").owlCarousel({
            navigation: true,
            loop: true,
            paginationSpeed: 400,
            autoplay: true,
            autoplayHoverPause: true,
            smartSpeed: 700,
            autoplayTimeout: 5000,
            responsiveClass: true,
            slideBy: 1,
            items: 3,
            dots: false,
            nav: true,
            margin: 30,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            responsive : {
                // breakpoint from 0 up
                0 : {
                    items: 1,
                },
                // breakpoint from 768 up
                768 : {
                    items: 2,
                },
                990 : {
                    items: 3,
                }
            }
        });

        $(".depoimentos-carousel").owlCarousel({
            navigation: true,
            loop: true,
            paginationSpeed: 300,
            autoplay: true,
            autoplayHoverPause: true,
            smartSpeed: 700,
            autoplayTimeout: 5000,
            responsiveClass: true,
            slideBy: 1,
            items: 1,
            dots: true,
            nav: false,
            margin: 30,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>']
        });

        //WOW Effect
        wow = new WOW(
            {
                boxClass: 'wow',      // default
                animateClass: 'animated', // default
                offset: 0,          // default
                mobile: true,       // default
                live: true        // default
            }
        );
        wow.init();
    });

    //Botão Menu Mobile
    $('.nav-toggler, .mobile-menu-overlay').click(function () {
        $('.nav-toggler').toggleClass('toggler-open');
        $('.mobile-menu-wrapper, .mobile-menu-overlay').toggleClass('open');
        $('body, #menu-secundario').toggleClass('arredar');
    });

    //Contagem Nossos Números
    $('#nossos-numeros').waypoint(function () {
        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 3000,
                easing: 'easeOutExpo',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $('.numeros').addClass('animated fadeIn faster');
        this.destroy()
    }, {offset: '50%'});

    $('.nav-link').removeAttr('title');

// Botão voltar ao topo
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#voltarTopo').addClass('show')
        } else {
            $('#voltarTopo').removeClass('show');
        }
    });
    $('#voltarTopo').click(function () {
        $("html, body").animate({scrollTop: 0}, 1200, 'easeInOutExpo');
        return false;
    });

    $(".view-products").click(function (event) {
        event.preventDefault();
        $('html,body').animate({
                scrollTop: $("#blocks").offset().top
            },
            1200, 'easeInOutExpo');
    });


    //Inicio do calculo de IMC
    $("#altura").mask("9.99"), $(".calcular").on("click", function (o) {
        o.preventDefault();
        if($('.input-imc').val() != ''){
        $('.imc').css('opacity', '1');
        var t = $("#peso").val(),
            e = $("#altura").val(),
            l = t / (e * e);
        $(".result").html(l.toFixed(2)), l < 18.5 ? $(".ruslt-text").html("O seu IMC é: <bold><span class='resposta-imc fw-semi-bold'>" + l.toFixed(2) + "</span></bold> </br>  Isso significa <bold><span class='resposta-imc fw-bold text-danger text-uppercase'> Abaixo do peso</span></bold>")
            : l >= 18.5 && l < 24.9 ? $(".ruslt-text").html("Seu IMC está <bold><span class='resposta-imc fw-semi-bold'>" + l.toFixed(2) + "</span></bold> </br>  Isso significa <bold><span class='resposta-imc fw-bold text-success text-uppercase'>  Peso Normal</span></bold>")
                : l >= 25 && l < 29.9 ? $(".ruslt-text").html("Seu IMC está <bold><span class='resposta-imc fw-semi-bold'>" + l.toFixed(2) + "</span></bold> </br>  Isso significa <bold><span class='resposta-imc fw-bold text-warning text-uppercase'> Sobrepeso</span></bold>")
                    : l >= 30 && l < 34.9 ? $(".ruslt-text").html("Seu IMC está <bold><span class='resposta-imc fw-semi-bold'>" + l.toFixed(2) + "</span></bold> </br>  Isso significa <bold><span class='resposta-imc fw-bold text-danger text-uppercase'> Obesidade Grau I</span></bold>")
                        : l >= 35 && l < 39.9 ? $(".ruslt-text").html("Seu IMC está <bold><span class='resposta-imc fw-semi-bold'>" + l.toFixed(2) + "</span></bold> </br>  Isso significa <bold><span class='resposta-imc fw-bold text-danger text-uppercase'> Obesidade Grau II</span></bold>")
                            : l > 40 && $(".ruslt-text").html("Seu IMC está <bold><span class='resposta-imc fw-semi-bold'>" + l.toFixed(2) + "</span></bold> </br>  Isso significa <bold><span class='resposta-imc fw-bold text-danger text-uppercase'> Obesidade Grau III</span></bold>")
        };
        });
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

})(jQuery);
